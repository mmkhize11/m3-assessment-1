import React,{useEffect, useState} from 'react'
import { StyleSheet, Text, ScrollView,View,StatusBar,Image,TextInput, TouchableOpacity } from 'react-native'
import {Colorstobeused} from '../constants/color'
import Icon from 'react-native-vector-icons/FontAwesome'
import Buttons from '../components/buttons'
import {Routes, Route, useNavigate} from 'react-router-dom';
// import "firebase/compat/auth";
// import "firebase/compat/firestore";
import '@firebase/auth';
import '@firebase/firestore';
import firebase from '../config';


export interface Props{
    navigation:any;
    // user:any;
    
}

const Login =  ({navigation}) => {

   


    React.useEffect(() => {
        const usersRef = firebase.firestore().collection('users');
        var user = firebase.auth().currentUser;
console.log(`what is user   ${JSON.stringify(user)}`)
        if(user!=null){
            usersRef
            .doc(user.uid)
            .get()
            .then((document) => {
              const userData = document.data()
              persistForm(userData)
              
            //   useEffect(function persistForm() {
            
            //     setLoading(false)
            //     setUser(userData)
            //   });
           
              navigation.replace('DashboardScreen')
            })
            .catch((error) => {
                persistForm(null)
          
            });
        }else{
            setLoading(false)
            setUser(null)
        }
     
        // firebase.auth().onAuthStateChanged(user => {
        //   if (user) {
        //     usersRef
        //       .doc(user.uid)
        //       .get()
        //       .then((document) => {
        //         const userData = document.data()
        //         persistForm(userData)
                
        //       //   useEffect(function persistForm() {
              
        //       //     setLoading(false)
        //       //     setUser(userData)
        //       //   });
             
        //         navigation.replace('DashboardScreen')
        //       })
        //       .catch((error) => {
        //           persistForm(null)
            
        //       });
        //   } else {
        //   //   setLoading(false)
        //   persistForm(null)
    
        //   }
        // });

   
      }, []);

    // render() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('');


    const [loading, setLoading] = useState(true)
    const [user, setUser] = useState(null)
  
    function persistForm(value) {
       
        // useEffect(() => {
            setLoading(false)
            setUser(value)
        // })
       
      }

      
  
    if (loading) {
      return (
        <></>
      )
    }

   
    const onLoginPress = () => {
        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then((response) => {
                const uid = response.user.uid
                const usersRef = firebase.firestore().collection('users')
                usersRef
                    .doc(uid)
                    .get()
                    .then(firestoreDocument => {
                        if (!firestoreDocument.exists) {
                            alert("User does not exist. Please ensure Details are entered correctly")
                            return;
                        }
                        const user = firestoreDocument.data()

                        // this.props.user=user;
                        // this.setState({
                        //     user: user
                        //   });
                        persistForm(user)
                       navigation.replace('DashboardScreen')
                    })
                    .catch(error => {
                        alert(error)
                    });
            })
            .catch(error => {
                alert(error)
            })
    }

    return (
        <ScrollView style={{flex:1,backgroundColor:'#fff',flexDirection:'column'}}>
            <StatusBar barStyle="dark-content" backgroundColor="#fff" />
            {/* login form section */}
            <View style={{flex:2,flexDirection:'column',backgroundColor:'#fff',paddingTop:10,paddingHorizontal:'3%'}} >
                <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}} >
                    <Text style={{fontFamily:'OpenSans-Bold',fontSize:30,color:Colorstobeused.black}} >Welcome</Text>
                    {/* <Image source={require('../assets/images/waving_hand.png')} style={{width:30,height:30}}  /> */}
                </View>
                {/* <Text style={{fontFamily:"OpenSans-Regular",fontSize:14,paddingTop:10,color:"#777"}} >We are happy to see you again. You can continue where you left off by logging in</Text> */}

                <View style={{flexDirection:'column',paddingTop:20}} >
                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#ededed',width:'95%',borderRadius:10,height:60,paddingLeft:20}} >
                        <Icon name="envelope-o" size={22} color="#818181" />
                        <TextInput  onChangeText={(text) => setEmail(text)} value={email} style={styles.input} placeholder="Enter Email" placeholderTextColor="#818181" />

                    </View>

                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#ededed',width:'95%',borderRadius:10,height:60,paddingLeft:20,marginTop:20}} >
                        <Icon name="lock" size={22} color="#818181" />
                        <TextInput onChangeText={(text) => setPassword(text)}  value={password} style={styles.input} placeholder="Enter Password" secureTextEntry={true} placeholderTextColor="#818181" />
                    </View>

                    <View style={{width:'95%',marginBottom:10}} >
                        <Text style={{fontSize:17,fontFamily:'OpenSans-Bold',
                    color:'#818181',alignSelf:'flex-end',paddingTop:10}} >Forgot Password?</Text>
                    </View>


                    <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'flex-end',backgroundColor:'#fff',marginBottom:40}} >
                    <Text style={{fontFamily:'OpenSans-Medium',fontSize:17,color:'#000000'}} >Don't have a account? </Text>
                    {/* <Text style={{fontSize:18,fontFamily:'OpenSans-SemiBold',color:'#f93b50'}}  >Sign Up</Text> */}
                    <Text style={{fontSize:18,fontFamily:'OpenSans-SemiBold',color:'#f93b50'}} onPress={ ()=> navigation.navigate('Signup') } >Sign Up</Text>
       
                    {/*  */}
                </View>
               
                    <Buttons  btn_text={"Sign In"} on_press={()=>onLoginPress()} />
                </View>
            </View>

        
        </ScrollView>
    )
// }
}

export default Login

const styles = StyleSheet.create({
    input:{
        position:'relative',
        height:'100%',
        width:'90%',
        fontFamily:'OpenSans-Regular',
        paddingLeft:20,
    },
    social_btn:{
        height:55,
        width:'100%',
        borderWidth:1,
        borderRadius:10,
        borderColor:'#ddd',
        flexDirection:'row',
        alignItems:'center',
        marginBottom:20
    },
    social_img:{
        width:25,
        height:25,
        marginLeft:15
    }
})