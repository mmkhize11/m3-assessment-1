import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { AppBar } from '@react-native-material/core';
import {  HStack } from 'native-base';
// import { FontAwesome } from 'react-native-vector-icons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/FontAwesome';
// import { IconButton } from "@react-native-material/core";
import { IconButton } from 'react-native-paper';
// import firebase from "firebase";
import "firebase/compat/storage";
import "firebase/compat/auth";
// import "firebase/firestore";

import firebase from '../config';
// import firestoreDocRef from '../config';
// import * as firebase from 'firebase/compat/app';
// import { initializeApp } from 'firebase/app';
import { getFirestore, setDoc, doc } from 'firebase/firestore';
import { Appbar, FAB, useTheme } from 'react-native-paper';
import {
  faEye,
  faSearch,
  faBars,
  faSmile,faArrowLeft
} from "@fortawesome/free-solid-svg-icons";
// let myApp = initializeApp(firebaseConfig);
export default function Scanner({navigation}) {


  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      // firebase.initializeApp(firebaseConfig);
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = async ({ type, data }) => {
    setScanned(true);
    // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    alert('Searching for discount')


    // const entityRef = firebase.collection('');
    
    const entityRef = firebase.firestore().collection('Discounts')
    // initializeApp
   
  //   var db = firestoreDocRef.firestore();
  //   const ref = db.collection('Discounts');
    
  entityRef.where("Barcode", '==', data)
  .get()
  .then((querySnapshot) => {
    querySnapshot.forEach((doc) => {


      if(doc.exists){


        firebase.auth().onAuthStateChanged(async user => {
          firebase.firestore().collection('users').doc(user.uid).collection('Discounts').add({
            Barcode: doc.data()['Barcode'],
            DiscountValue:doc.data()['DiscountValue'],
            ID: doc.data()['ID'],
            id:doc.id
          });
  
       })
     
    

alert(`Congratulations you have received a R ${doc.data()['DiscountValue']} Discount`);
      

}else if(!doc.exists){
        alert(`Please scan another bar code `);
      }
      // console.log(doc.id, "=>", doc.data());
    });

  });

  //   firestore
  // .collection('Discounts')
  // // Filter results
  // .where('Barcode', '=', '9781736230909')
  // .get()
  // .then(querySnapshot => {
  //   console.log(`what is the response   ${ JSON.stringify(querySnapshot)}`)
  //   /* ... */
  // });


  };

  if (hasPermission === null) {
    return <Text style={{textAlignVertical: "center",textAlign: "center",}}>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text style={{textAlignVertical: "center",textAlign: "center",}}>No access to camera</Text>;
  }

  

  return (
    <View style={styles.container}>
      <AppBar
            style={{backgroundColor:'#f93b50',position: 'absolute',
            top: 0,
            left: 0,
            right: 0}} 
           
    title="Discount Scanner "
    leading={props => (
        <IconButton
        icon="arrow-left"
        color={ '#ffffff'}
        // color={Colors.red500}
        size={20}
        onPress={() =>  navigation.replace('DiscountList')}
      />
    //   <IconButton icon={props => <Icon name="menu" {...props} />} {...props} />
    )}
    trailing={props => (
      <HStack>
        
       
      </HStack>
    )}
  />
 {/* <Appbar.Header
                style={{backgroundColor:'#f93b50',position: 'absolute',
                top: 0,
                left: 0,
                right: 0}} 
      >

<Appbar.Action icon="arrow-left"  onPress={() =>  navigation.replace('DashboardScreen')}/>
<Appbar.Content title="Discount Scanner " onPress={() => {}} />

      
    
  
    

      </Appbar.Header> */}


      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});